Лабы 1 и 2 используют PostgreSQL
```
createdb department
psql department <db_dump.sql
```

Создайте файл `src/main/resources/database.properties` и укажите в нём настройки подключения к БД.
Он используется в обеих лабах.
```
url=jdbc:postgresql://localhost/department
username=user
password=pass
# for lab 1
ssl=false
```

Код лабы 1 в коммите с тегом `lab1`.

Вариант 21. *Команда разработчиков*.