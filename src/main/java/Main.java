import models.Bill;
import models.Customer;
import models.Developer;
import models.Project;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;

public class Main {
    static DAO dao;

    public static void printHelp() {
        System.out.println("[h]elp - display this message");
        System.out.println("[q]uit - quit interactive session");
        System.out.println("[c]ustomer <id>|* - get customer by id or all customers");
        System.out.println("[b]ill <project_id customer_id>|* - get bill by id or all bills");
        System.out.println("[pr]oject <id>|* - get project by id or all projects");
        System.out.println("[d]eveloper <id>|* - get developer by id or all developers");
        System.out.println("[pa]y <project_id> <customer_id> - pay customer bill for project");
    }

    public static void showMenu() throws SQLException, IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String answer;
        while (true) {
            answer = bufferedReader.readLine();
            String command[] = answer.split(" ", 2);

            if (answer.length() == 0)
                continue;

            if ("help".startsWith(answer))
                printHelp();
            else if ("quit".startsWith(answer))
                break;

            else if ("customer".startsWith(command[0])) {
                try {
                    if (command[1].equals("*")) {
                        ArrayList<Customer> customers = dao.getAllCustomers();
                        System.out.println(customers);
                    } else {
                        int id = Integer.parseInt(command[1]);
                        Customer c = dao.getCustomerByID(id);
                        System.out.println(c);
                    }
                } catch (Exception e) {
                    System.out.println(e);
                    System.out.println("usage: customer <id>|*");
                }
            } else if ("bill".startsWith(command[0])) {
                try {
                    if (command[1].equals("*")) {
                        ArrayList<Bill> bills = dao.getAllBills();
                        System.out.println(bills);
                    } else {
                        String args[] = command[1].split(" ");
                        int project_id = Integer.parseInt(args[0]);
                        int customer_id = Integer.parseInt(args[1]);
                        Bill b = dao.getBillByID(project_id, customer_id);
                        System.out.println(b);
                    }
                } catch (Exception e) {
                    System.out.println("usage: bill <project_id customer_id>|*");
                }
            } else if (command[0].length() > 1 && "project".startsWith(command[0])) {
                try {
                    if (command[1].equals("*")) {
                        ArrayList<Project> projects = dao.getAllProjects();
                        System.out.println(projects);
                    } else {
                        int id = Integer.parseInt(command[1]);
                        Project p = dao.getProjectByID(id);
                        System.out.println(p);
                    }
                } catch (Exception e) {
                    System.out.println("usage: project <id>|*");
                }
            } else if ("developer".startsWith(command[0])) {
                try {
                    if (command[1].equals("*")) {
                        ArrayList<Developer> developers = dao.getAllDevelopers();
                        System.out.println(developers);
                    } else {
                        int id = Integer.parseInt(command[1]);
                        Developer d = dao.getDeveloperByID(id);
                        System.out.println(d);
                    }
                } catch (Exception e) {
                    System.out.println("usage: developer <id>|*");
                }
            } else if (command[0].length() > 1 && "pay".startsWith(command[0])) {
                try {
                    String args[] = command[1].split(" ");
                    int project_id = Integer.parseInt(args[0]);
                    int customer_id = Integer.parseInt(args[1]);
                    Bill b = dao.getBillByID(project_id, customer_id);
                    Customer c = dao.getCustomerByID(customer_id);

                    if (b == null) {
                        System.out.println("no such bill");
                        continue;
                    }
                    if (c == null) {
                        System.out.println("no such customer");
                        continue;
                    }

                    double sumToPay = Double.max(b.getAmount() - b.getPaid(), 0);
                    if (c.getMoney() <= 0)
                        System.out.println("customer doesn't have money");
                    else if (b.getPaid() >= b.getAmount())
                        System.out.println("bill is already paid");
                    else {
                        double sumPaid = Double.min(sumToPay, c.getMoney());
                        System.out.println(sumPaid + " paid by customer " + c + " for bill " + b);
                        c.setMoney(c.getMoney() - sumPaid);
                        b.setPaid(b.getPaid() + sumPaid);
                        int customersUpdated = dao.updateCustomer(c);
                        int billsUpdated = dao.updateBill(b);
                        System.out.println(customersUpdated + " customer updated");
                        System.out.println(billsUpdated + " bill updated");
                    }
                } catch (Exception e) {
                    System.out.println("usage: pay <project_id> <customer_id>");
                }
            } else {
                System.out.println("unknown command");
            }
        }
    }

    public static void main(String[] args) throws SQLException, IOException {
        dao = new DAO();

        printHelp();
        showMenu();
    }
}
