import models.Bill;
import models.Customer;
import models.Developer;
import models.Project;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.sql.SQLException;
import java.util.ArrayList;

public class DAO {
    private static final String persistenceName = "department";
    private static final EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory(persistenceName);
    private EntityManager entityManager = entityManagerFactory.createEntityManager();

    public Developer getDeveloperByID(int developer_id) throws SQLException {
        return entityManager
                .createNamedQuery("Developer.getByID", Developer.class)
                .setParameter("id", developer_id)
                .getSingleResult();
    }

    public ArrayList<Developer> getAllDevelopers() throws SQLException {
        return new ArrayList<>(entityManager
                .createNamedQuery("Developer.getAll", Developer.class)
                .getResultList());
    }

    public Project getProjectByID(int project_id) throws SQLException {
        return entityManager
                .createNamedQuery("Project.getByID", Project.class)
                .setParameter("id", project_id)
                .getSingleResult();
    }

    public ArrayList<Project> getAllProjects() throws SQLException {
        return new ArrayList<>(entityManager
                .createNamedQuery("Project.getAll", Project.class)
                .getResultList());
    }

    public Customer getCustomerByID(int customer_id) throws SQLException {
        return entityManager
                .createNamedQuery("Customer.getByID", Customer.class)
                .setParameter("id", customer_id)
                .getSingleResult();
    }

    public int updateCustomer(Customer c) throws SQLException {
        EntityTransaction updateTransaction = entityManager
                .getTransaction();
        updateTransaction.begin();
        int count = entityManager.createNamedQuery("Customer.update")
                .setParameter("id", c.getCustomerID())
                .setParameter("money", c.getMoney())
                .setParameter("name", c.getName())
                .executeUpdate();
        updateTransaction.commit();
        return count;
    }

    public ArrayList<Customer> getAllCustomers() throws SQLException {
        return new ArrayList<>(entityManager
                .createNamedQuery("Customer.getAll", Customer.class)
                .getResultList());
    }

    public Bill getBillByID(int project_id, int customer_id) throws SQLException {
        return entityManager
                .createNamedQuery("Bill.getByIDs", Bill.class)
                .setParameter("project_id", project_id)
                .setParameter("customer_id", customer_id)
                .getSingleResult();
    }

    public int updateBill(Bill b) throws SQLException {
        EntityTransaction updateTransaction = entityManager
                .getTransaction();
        updateTransaction.begin();
        int count = entityManager.createNamedQuery("Bill.update")
                .setParameter("customer_id", b.getCustomerId())
                .setParameter("project_id", b.getProjectId())
                .setParameter("amount", b.getAmount())
                .setParameter("paid", b.getPaid())
                .executeUpdate();
        updateTransaction.commit();
        return count;
    }

    public ArrayList<Bill> getAllBills() throws SQLException {
        return new ArrayList<>(entityManager
                .createNamedQuery("Bill.getAll", Bill.class)
                .getResultList());
    }

}
