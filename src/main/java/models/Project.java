package models;

import javax.persistence.*;

@Entity
@Table(name = "project")
@NamedQueries({
        @NamedQuery(name = "Project.getByID", query = "SELECT p FROM Project p WHERE project_id = :id"),
        @NamedQuery(name = "Project.getAll", query = "SELECT p FROM Project p")
})
public class Project {
    @Id
    private int project_id;
    @Column(name = "customer_id")
    private int customer_id;
    @Column(name = "name")
    private String name;

    public Project() {}

    public Project(int project_id, int customer_id, String name) {
        this.project_id = project_id;
        this.customer_id = customer_id;
        this.name = name;
    }

    public int getProjectId() {
        return project_id;
    }

    public int getCustomerId() {
        return customer_id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Project{" +
                "project_id=" + project_id +
                ", customer_id=" + customer_id +
                ", name='" + name + '\'' +
                '}';
    }
}
