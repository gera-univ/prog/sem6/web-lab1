package models;

import javax.persistence.*;
import java.io.Serializable;

class BillIDs implements Serializable {
    private int project_id;
    private int customer_id;
}

@Entity
@IdClass(BillIDs.class) // so we can have two ids
@Table(name = "bill")
@NamedQueries({
        @NamedQuery(name = "Bill.getByIDs", query = "SELECT b FROM Bill b WHERE project_id = :project_id and customer_id = :customer_id"),
        @NamedQuery(name = "Bill.getAll", query = "SELECT b FROM Bill b"),
        @NamedQuery(name = "Bill.update", query = "UPDATE Bill b SET amount = :amount, paid = :paid WHERE customer_id = :customer_id and project_id = :project_id"),
})
public class Bill {
    @Id
    private int project_id;
    @Id
    private int customer_id;
    @Column(name = "amount")
    private double amount;
    @Column(name = "paid")
    private double paid;

    public Bill() {
    }

    public Bill(int project_id, int customer_id, double amount, double paid) {
        this.project_id = project_id;
        this.customer_id = customer_id;
        this.amount = amount;
        this.paid = paid;
    }

    public int getProjectId() {
        return project_id;
    }

    public int getCustomerId() {
        return customer_id;
    }

    public double getAmount() {
        return amount;
    }

    public double getPaid() {
        return paid;
    }

    public void setPaid(double paid) {
        this.paid = paid;
    }

    @Override
    public String toString() {
        return "Bill{" +
                "project_id=" + project_id +
                ", customer_id=" + customer_id +
                ", amount=" + amount +
                ", paid=" + paid +
                '}';
    }
}
