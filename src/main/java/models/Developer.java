package models;

import javax.persistence.*;

@Entity
@Table(name = "developer")
@NamedQueries({
        @NamedQuery(name = "Developer.getByID", query = "SELECT d FROM Developer d WHERE developer_id = :id"),
        @NamedQuery(name = "Developer.getAll", query = "SELECT d FROM Developer d")
})
public class Developer {
    @Id
    private int developer_id;
    @Column(name = "project_id")
    private int project_id;
    @Column(name = "name")
    private String name;

    public Developer() {}

    public Developer(int id, int project_id, String name) {
        this.developer_id = id;
        this.project_id = project_id;
        this.name = name;
    }

    public int getDeveloperId() {
        return developer_id;
    }

    public int getProjectId() {
        return project_id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Developer{" +
                "developer_id=" + developer_id +
                ", project_id=" + project_id +
                ", name='" + name + '\'' +
                '}';
    }
}
