package models;
import javax.persistence.*;

@Entity
@Table(name = "customer")
@NamedQueries({
        @NamedQuery(name = "Customer.getByID", query = "SELECT c FROM Customer c WHERE customer_id = :id"),
        @NamedQuery(name = "Customer.getAll", query = "SELECT c FROM Customer c"),
        @NamedQuery(name = "Customer.update", query = "UPDATE Customer c SET name = :name, money = :money WHERE customer_id = :id"),
})
public class Customer {
    @Id
    private int customer_id;
    @Column(name = "name")
    private String name;
    @Column(name = "money")
    private double money;

    public Customer() {}

    public Customer(int customer_id, String name, double money) {
        this.customer_id = customer_id;
        this.name = name;
        this.money = money;
    }

    public int getCustomerID() {
        return customer_id;
    }

    public String getName() {
        return name;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "customer_id=" + customer_id +
                ", name='" + name + '\'' +
                ", money=" + money +
                '}';
    }
}
